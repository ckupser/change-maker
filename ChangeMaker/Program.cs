﻿using System;

namespace ChangeMaker
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.Write("Enter a value in US Dollars and cents (e.g. \"13.22\"):  $");
            var input = Console.ReadLine().Trim();

            if (!decimal.TryParse(input, out var parsedInput))
            {
                Console.WriteLine("Invalid input.");
            }
            else
            {
                try
                {
                    var result = new Coins(parsedInput);

                    Console.WriteLine($"Dollar coins:  {result.Dollars}");
                    Console.WriteLine($"Quarters:  {result.Quarters}");
                    Console.WriteLine($"Dimes:  {result.Dimes}");
                    Console.WriteLine($"Nickels:  {result.Nickels}");
                    Console.WriteLine($"Pennies:  {result.Pennies}");
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message);
                }
            }

            Console.WriteLine("Press Enter to quit.");
            Console.ReadLine();
        }
    }
}
﻿using System;

namespace ChangeMaker
{
    public class Coins
    {
        private const decimal DollarAmount = 1;

        private const decimal QuarterAmount = 0.25m;

        private const decimal DimeAmount = 0.1m;

        private const decimal NickelAmount = 0.05m;

        private const decimal PennyAmount = 0.01m;

        public int Dollars { get; set; }

        public int Quarters { get; set; }

        public int Dimes { get; set; }

        public int Nickels { get; set; }

        public int Pennies { get; set; }

        public Coins(decimal amount)
        {
            if (amount < 0)
            {
                throw new ArgumentException($"Amount must be greater than $0.  Amount given:  {amount}");
            }

            if (amount % 0.01m > 0)
            {
                throw new ArgumentException($"No coin exists for amounts less than $0.01.  Amount given:  {amount}.");
            }

            Dollars = DetermineNumberOfCoins(amount, DollarAmount);
            amount -= Dollars * DollarAmount;

            Quarters = DetermineNumberOfCoins(amount, QuarterAmount);
            amount -= Quarters * QuarterAmount;

            Dimes = DetermineNumberOfCoins(amount, DimeAmount);
            amount -= Dimes * DimeAmount;

            Nickels = DetermineNumberOfCoins(amount, NickelAmount);
            amount -= Nickels * NickelAmount;

            Pennies = DetermineNumberOfCoins(amount, PennyAmount);
        }

        private static int DetermineNumberOfCoins(decimal total, decimal coinValue) => (int)(total / coinValue);
    }
}
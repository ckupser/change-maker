Requires .NET Core 2.2+.  Available at https://dotnet.microsoft.com/download.

To run in Windows:  execute Run.bat.
To run in MacOS or Linux:  enter "dotnet run --project ./ChangeMaker" in a terminal at the repository root.
using System;
using Xunit;

namespace ChangeMaker.Tests.Unit
{
    public class CoinsTests
    {
        [Fact]
        public void NegativeAmount_ThrowsArgumentException() => Assert.Throws<ArgumentException>(() => new Coins(-1));

        [Fact]
        public void AmountContainingLessThanAPenny_ThrowsArgumentException() => Assert.Throws<ArgumentException>(() => new Coins(1.001m));

        [Fact]
        public void ZeroAmount_ReturnsNoCoins()
        {
            var response = new Coins(0);

            Assert.Equal(0, response.Dollars);
            Assert.Equal(0, response.Quarters);
            Assert.Equal(0, response.Dimes);
            Assert.Equal(0, response.Nickels);
            Assert.Equal(0, response.Pennies);
        }

        [Fact]
        public void WholeDollars_AreAddedToDollarCoinCount()
        {
            const int dollars = 1;

            var response = new Coins(dollars);

            Assert.Equal(dollars, response.Dollars);
        }

        [Fact]
        public void AmountOf0_25_IsAddedToDollarCoinCount()
        {
            var response = new Coins(0.25m);

            Assert.Equal(1, response.Quarters);
        }

        [Fact]
        public void AmountOf0_10_IsAddedToDimeCoinCount()
        {
            var response = new Coins(0.1m);

            Assert.Equal(1, response.Dimes);
        }

        [Fact]
        public void AmountOf0_05_IsAddedToNickelCoinCount()
        {
            var response = new Coins(0.05m);

            Assert.Equal(1, response.Nickels);
        }

        [Fact]
        public void AmountOf0_01_IsAddedToPennyCount()
        {
            var response = new Coins(0.01m);

            Assert.Equal(1, response.Pennies);
        }

        [Fact]
        public void MixedCoinAmount_SplitsIntoCorrectCoins()
        {
            var response = new Coins(12.93m);

            Assert.Equal(12, response.Dollars);
            Assert.Equal(3, response.Quarters);
            Assert.Equal(1, response.Dimes);
            Assert.Equal(1, response.Nickels);
            Assert.Equal(3, response.Pennies);
        }
    }
}